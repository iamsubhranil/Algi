#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// LLVM Core
#include <llvm-c/Analysis.h>
#include <llvm-c/Core.h>
#include <llvm-c/ExecutionEngine.h>
#include <llvm-c/OrcBindings.h>
#include <llvm-c/Target.h>
#include <llvm-c/Types.h>

// Optimizations
#include <llvm-c/Transforms/IPO.h>
#include <llvm-c/Transforms/PassManagerBuilder.h>
#include <llvm-c/Transforms/Scalar.h>
#include <llvm-c/Transforms/Vectorize.h>

#include "codegen.h"
#include "display.h"
#include "expr.h"
#include "runtime.h"
#include "stmt.h"
#include "timer.h"
#include "types.h"

typedef struct {
	LLVMValueRef ref;
	uint64_t     hash;
} VariableRef;

// static uint64_t variableRefPointer = 0;
// static VariableRef *variables = NULL;

#define MAX_VARIABLES 255

typedef struct AlgiContext {
	VariableRef variables[MAX_VARIABLES];
	uint8_t     variableRefPointer;

	struct AlgiContext *superContext;
} AlgiContext;

static uint8_t     algiGlobalContextInit = 0;
static AlgiContext algiGlobalContext;

static LLVMTypeRef get_generic_structure_type() {
	LLVMTypeRef types[] = {LLVMInt8Type(), LLVMInt64Type()};
	return LLVMStructType(types, 2, 0);
}

// static LLVMValueRef get_container_structure(){
//    LLVMValueRef
//}

static uint64_t hash(const char *str, uint64_t length) {
	uint64_t hash = 5381;
	uint64_t c    = 0;
	//    printf("\nHashing..\n");
	while(c < length)
		hash = ((hash << 5) + hash) + str[c++]; /* hash * 33 + c */
	//    printf("\nInput String : [%s] Hash : %lu\n", str, hash);
	return hash;
}

static LLVMExecutionEngineRef globalEngine;
static LLVMBuilderRef         globalBuilder;
static LLVMContextRef         llvmGlobalContext;
static LLVMModuleRef          globalModule;

static LLVMValueRef get_variable_ref(Expression *varE, AlgiContext *context,
                                     uint8_t declareIfNotfound) {
	if(context == NULL)
		return NULL;

	varE->hash = hash(varE->token.string, varE->token.length);
	// dbg("Searching for hash %ld at context %p", varE->hash, context);
	for(uint64_t i = 0; i < context->variableRefPointer; i++) {
		if(context->variables[i].hash == varE->hash) {
			LLVMValueRef ret = context->variables[i].ref;
			// dbg("Declared as %s", LLVMPrintValueToString(ret));
			return ret;
		}
	}

	LLVMValueRef superType = get_variable_ref(varE, context->superContext, 0);
	if(superType != NULL) {
		return superType;
	}

	if(declareIfNotfound) {
		// dbg("Declaring ");
		// lexer_print_token(varE->token, 0);
		// dbg(" at context %p", context);
		context->variableRefPointer++;
		// variables = (VariableRef *)realloc(variables, sizeof(VariableRef) *
		// variableRefPointer);
		LLVMValueRef ref;
		switch(varE->valueType) {
			case VALUE_GEN:
				ref =
				    LLVMBuildAlloca(globalBuilder, get_generic_structure_type(),
				                    "localGeneric");
				break;
			case VALUE_STR:
				ref = LLVMBuildAlloca(globalBuilder,
				                      LLVMPointerType(LLVMInt8Type(), 0),
				                      "localString");
				break;
			case VALUE_NUM:
				ref = LLVMBuildAlloca(globalBuilder, LLVMDoubleType(),
				                      "localDouble");
				break;
			case VALUE_INT:
				ref =
				    LLVMBuildAlloca(globalBuilder, LLVMInt64Type(), "localInt");
				break;
			case VALUE_STRUCT:
				ref =
				    LLVMBuildAlloca(globalBuilder, get_generic_structure_type(),
				                    "localContainer");
				break;
			case VALUE_BOOL:
				ref =
				    LLVMBuildAlloca(globalBuilder, LLVMInt1Type(), "localBool");
				break;
		}
		context->variables[context->variableRefPointer - 1].ref  = ref;
		context->variables[context->variableRefPointer - 1].hash = varE->hash;
		return ref;
	}
	return NULL;
}

// Marks which runtime functions have been used in the program
// to perform the global mapping of their physical address
// to the engine
static uint8_t runtime_function_used[ALGI_RUNTIME_FUNCTION_COUNT] = {0};

static ValueType convert_llvmtype_to_algitype(LLVMTypeRef type) {
	if(type == get_generic_structure_type())
		return VALUE_GEN;
	if(type == LLVMInt1Type())
		return VALUE_BOOL;
	if(type == LLVMInt64Type())
		return VALUE_INT;
	if(type == LLVMDoubleType())
		return VALUE_NUM;
	return VALUE_STR;
}

static LLVMValueRef build_cast_call(LLVMValueRef val, ValueType castType) {
	LLVMTypeRef argumentType[2];
	argumentType[0] = LLVMInt32Type();

	LLVMValueRef argumentValue[2];

	LLVMTypeRef returnType, valueType;
	const char *callee = "__algi_invalid_cast";
	if(LLVMIsAAllocaInst(val)) {
		valueType = LLVMGetAllocatedType(val);
		val       = LLVMBuildLoad(globalBuilder, val, "loadtmp");
	} else {
		valueType = LLVMTypeOf(val);
	}

	argumentType[1]  = valueType;
	argumentValue[1] = val;

	ValueType algiType = convert_llvmtype_to_algitype(valueType);
	switch(castType) {
		case VALUE_INT: {
			runtime_function_used[ALGI_TO_INT] = 1;
			callee                             = "__algi_to_int";
			returnType                         = LLVMInt64Type();
		} break;
		case VALUE_NUM: {
			runtime_function_used[ALGI_TO_DOUBLE] = 1;
			callee                                = "__algi_to_double";
			returnType                            = LLVMDoubleType();
		} break;
		case VALUE_STR: {
			runtime_function_used[ALGI_TO_STRING] = 1;
			callee                                = "__algi_to_string";
			returnType = LLVMPointerType(LLVMInt8Type(), 0);
		} break;
		case VALUE_BOOL: {
			runtime_function_used[ALGI_TO_BOOLEAN] = 1;
			callee                                 = "__algi_to_boolean";
			returnType                             = LLVMInt1Type();
		} break;
		default: {
			algiType   = VALUE_UND;
			returnType = LLVMVoidType();
		} break;
	}
	if(algiType == VALUE_STR &&
	   LLVMGetTypeKind(valueType) == LLVMArrayTypeKind) {
		dbg("Changing string type for val : \n");
		LLVMDumpValue(val);
		size_t l;
		val = LLVMBuildGlobalString(globalBuilder, LLVMGetAsString(val, &l),
		                            "__runtime_call_tmp");
		argumentValue[1] = val;
	} else if(algiType == VALUE_GEN) {
		argumentValue[0] =
		    LLVMBuildExtractValue(globalBuilder, val, 0, "genextrct0");
		argumentValue[0]  = LLVMBuildIntCast(globalBuilder, argumentValue[0],
                                            LLVMInt32Type(), "gen0cast");
		LLVMValueRef cons = LLVMConstInt(LLVMInt32Type(), VALUE_GEN, 0);
		argumentValue[0] =
		    LLVMBuildAdd(globalBuilder, argumentValue[0], cons, "gentype");
		argumentValue[1] =
		    LLVMBuildExtractValue(globalBuilder, val, 1, "genextrct1");
	}
	if(algiType != VALUE_GEN)
		argumentValue[0] = LLVMConstInt(LLVMInt32Type(), algiType, 0);
	LLVMTypeRef  f  = LLVMFunctionType(returnType, argumentType, 1, 1);
	LLVMValueRef fn = LLVMGetNamedFunction(globalModule, callee);
	if(fn == NULL) {
		fn = LLVMAddFunction(globalModule, callee, f);
	}
	return LLVMBuildCall(globalBuilder, fn, argumentValue, 2,
	                     "__algi_internal_cast_res");
}

static LLVMValueRef compile_constant(Expression *e) {
	switch(e->token.type) {
		case TOKEN_integer:
			return LLVMConstInt(LLVMInt64Type(), e->consex.ival, 0);
		case TOKEN_number:
			return LLVMConstReal(LLVMDoubleType(), e->consex.dval);
		case TOKEN_string:
			return LLVMConstString(e->consex.sval, strlen(e->consex.sval), 0);
		case TOKEN_True: return LLVMConstInt(LLVMInt1Type(), 1, 1);
		case TOKEN_False: return LLVMConstInt(LLVMInt1Type(), 0, 1);
		default: return LLVMConstNull(LLVMInt1Type());
	}
}

static LLVMValueRef expr_compile(Expression *e);

static LLVMValueRef compile_unary(Expression *e) {
	LLVMValueRef expVal = expr_compile(e->unex.right);
	LLVMTypeRef  t      = LLVMTypeOf(expVal);
	if(LLVMIsAAllocaInst(expVal)) {
		t      = LLVMGetAllocatedType(expVal);
		expVal = LLVMBuildLoad(globalBuilder, expVal, "tmpLoad");
	}
#define ISINT() if(t == LLVMInt64Type() || t == LLVMInt1Type())
#define ISFLT() if(t == LLVMDoubleType())
#define ISBOOL() if(t == LLVMInt1Type())
	switch(e->token.type) {
		case TOKEN_minus:
			ISINT()
			return LLVMBuildNeg(globalBuilder, expVal, "negtmp");
			else ISFLT() return LLVMBuildFNeg(globalBuilder, expVal, "fnegtmp");
			return build_cast_call(expVal, VALUE_NUM);
		case TOKEN_not:
			ISBOOL()
			return LLVMBuildNot(globalBuilder, expVal, "nottmp");
			return build_cast_call(expVal, VALUE_BOOL);
		case TOKEN_Integer:
			ISINT() {
				ISBOOL()
				return LLVMBuildZExt(globalBuilder, expVal, LLVMInt64Type(),
				                     "bcastint");
				return expVal;
			}
			else ISFLT() return LLVMBuildFPToSI(globalBuilder, expVal,
			                                    LLVMInt64Type(), "fcasttmp");
			return build_cast_call(expVal, VALUE_INT);
		case TOKEN_Number:
			ISINT() {
				ISBOOL()
				return LLVMBuildUIToFP(globalBuilder, expVal, LLVMDoubleType(),
				                       "bdoublecasttmp");
				return LLVMBuildSIToFP(globalBuilder, expVal, LLVMDoubleType(),
				                       "doublecasttmp");
			}
			else ISFLT() return expVal;

			return build_cast_call(expVal, VALUE_NUM);
			// case TOKEN_Structure:
			//     return LLVMBuildPointerCast(globalBuilder, expVal,
			//     LLVMInt64Type(), "pointercasttmp");
		case TOKEN_Boolean:
			ISINT() {
				ISBOOL()
				return expVal;
				return LLVMBuildIntCast(globalBuilder, expVal, LLVMInt1Type(),
				                        "boolcasttmp");
			}
			return build_cast_call(expVal, VALUE_BOOL);
		case TOKEN_String: return build_cast_call(expVal, VALUE_STR);
		case TOKEN_Type:
			if(t == get_generic_structure_type()) {
				expVal = LLVMBuildExtractValue(globalBuilder, expVal, 0,
				                               "typeextract");
				return LLVMBuildIntCast(globalBuilder, expVal, LLVMInt64Type(),
				                        "typecnv");
			}
			return LLVMConstInt(LLVMInt64Type(), e->valueType, 0);
		default:
			// TODO: Handle this properly
			return LLVMConstNull(LLVMInt1Type());
#undef ISNUM
	}
}

static LLVMValueRef compile_binary(Expression *e) {
	LLVMValueRef left      = expr_compile(e->binex.left);
	LLVMValueRef right     = expr_compile(e->binex.right);
	LLVMTypeRef  leftType  = LLVMVoidType();
	LLVMTypeRef  rightType = LLVMVoidType();
	if(LLVMIsAAllocaInst(left)) {
		leftType = LLVMGetAllocatedType(left);
		left     = LLVMBuildLoad(globalBuilder, left, "tmpLoad");
	} else
		leftType = LLVMTypeOf(left);
	if(LLVMIsAAllocaInst(right)) {
		rightType = LLVMGetAllocatedType(right);
		right     = LLVMBuildLoad(globalBuilder, right, "tmpLoad");
	} else
		rightType = LLVMTypeOf(right);
	if((leftType != LLVMInt64Type()) || (rightType != LLVMInt64Type())) {
		if(leftType == LLVMInt64Type()) {
			left = LLVMBuildSIToFP(globalBuilder, left, LLVMDoubleType(),
			                       "dconvtmp");
		} else
			right = LLVMBuildSIToFP(globalBuilder, right, LLVMDoubleType(),
			                        "dconvtmp");
	}
#define IFINT() if(leftType == LLVMInt64Type() && rightType == LLVMInt64Type())
#define IFTYPECHECK(insIfInt, tempName)                                   \
	IFINT()                                                               \
	return LLVMBuild##insIfInt(globalBuilder, left, right, "i" tempName); \
	return LLVMBuildF##insIfInt(globalBuilder, left, right, "f" tempName);
	switch(e->token.type) {
		case TOKEN_plus: IFTYPECHECK(Add, "addtmp")
		case TOKEN_minus: IFTYPECHECK(Sub, "subtmp")
		case TOKEN_star: IFTYPECHECK(Mul, "multmp")
		case TOKEN_backslash:
			IFINT()
			return LLVMBuildSDiv(globalBuilder, left, right, "idivtmp");
			return LLVMBuildFDiv(globalBuilder, left, right, "fdivtmp");
		case TOKEN_greater:
			IFINT() {
				return LLVMBuildICmp(globalBuilder, LLVMIntSGT, left, right,
				                     "igttmp");
			}
			else return LLVMBuildFCmp(globalBuilder, LLVMRealOGT, left, right,
			                          "fgttmp");
		case TOKEN_greater_equal:
			IFINT() {
				return LLVMBuildICmp(globalBuilder, LLVMIntSGE, left, right,
				                     "igetmp");
			}
			else return LLVMBuildFCmp(globalBuilder, LLVMRealOGE, left, right,
			                          "fgetmp");
		case TOKEN_lesser:
			IFINT() {
				return LLVMBuildICmp(globalBuilder, LLVMIntSLT, left, right,
				                     "ilttmp");
			}
			else {
				// dbg("%s %s\n", LLVMPrintValueToString(left),
				//        LLVMPrintValueToString(right));
				// LLVMDumpModule(globalModule);
				// dbg("\n");
				return LLVMBuildFCmp(globalBuilder, LLVMRealOLT, left, right,
				                     "flttmp");
			}
		case TOKEN_lesser_equal:
			IFINT() {
				return LLVMBuildICmp(globalBuilder, LLVMIntSLE, left, right,
				                     "iletmp");
			}
			else return LLVMBuildFCmp(globalBuilder, LLVMRealOLE, left, right,
			                          "fletmp");
		case TOKEN_equal_equal:
			IFINT() {
				return LLVMBuildICmp(globalBuilder, LLVMIntEQ, left, right,
				                     "ieqtmp");
			}
			else return LLVMBuildFCmp(globalBuilder, LLVMRealOEQ, left, right,
			                          "feqtmp");
		case TOKEN_not_equal:
			IFINT() {
				return LLVMBuildICmp(globalBuilder, LLVMIntNE, left, right,
				                     "inetmp");
			}
			else return LLVMBuildFCmp(globalBuilder, LLVMRealONE, left, right,
			                          "fnetmp");
		default: // TOKEN_cap
		{
			if(LLVMTypeOf(left) != LLVMDoubleType()) {
				left = LLVMBuildSIToFP(globalBuilder, left, LLVMDoubleType(),
				                       "dcasttmp");
			} else
				LLVMDumpValue(left);
			if(LLVMTypeOf(right) != LLVMDoubleType()) {
				right = LLVMBuildSIToFP(globalBuilder, right, LLVMDoubleType(),
				                        "dcasttmp");
			}
			LLVMValueRef fn = LLVMGetNamedFunction(globalModule, "pow");
			if(fn == NULL) {
				LLVMTypeRef params[] = {LLVMDoubleType(), LLVMDoubleType()};
				LLVMTypeRef ret      = LLVMDoubleType();
				LLVMTypeRef fType    = LLVMFunctionType(ret, params, 2, 0);
				fn = LLVMAddFunction(globalModule, "pow", fType);
			}
			LLVMValueRef ref[] = {left, right};
			return LLVMBuildCall(globalBuilder, fn, ref, 2,
			                     "__algi_internal_pow_res");
		}
	}
}

static LLVMValueRef expr_compile(Expression *e) {
	if(e == NULL)
		return LLVMConstNull(LLVMInt64Type());
	switch(e->type) {
		case EXPR_CONSTANT: {
			return compile_constant(e);
		}
		case EXPR_UNARY: {
			return compile_unary(e);
		}
		case EXPR_VARIABLE:
			// dbg("Getting variable reference");
			return get_variable_ref(e, &algiGlobalContext, 1);
		case EXPR_BINARY: {
			return compile_binary(e);
		}
		case EXPR_REFERENCE: {
			// LLVMValueRef ref = get_variable_ref(e, 0);
			return LLVMConstNull(LLVMInt1Type());
		}
		default: // EXPR_CALL. EXPR_DEFINE will be handled by the statements
		{
			LLVMValueRef args[e->calex.arity];
			for(uint64_t i = 0; i < e->calex.arity; i++) {
				args[i] = expr_compile(e->calex.args[i]);
			}
			char *fName = (char *)malloc(e->token.length + 1);
			strncpy(fName, e->token.string, e->token.length);
			fName[e->token.length] = 0;
			LLVMValueRef fn        = LLVMGetNamedFunction(globalModule, fName);
			return LLVMBuildCall(globalBuilder, fn, args, e->calex.arity,
			                     "__algi_internal_func_res");
		}
	}
}

static LLVMValueRef blockstmt_compile(BlockStatement, uint8_t,
                                      LLVMBasicBlockRef);
static LLVMValueRef statement_compile(Statement *, LLVMBasicBlockRef);

static LLVMValueRef compile_set(Statement *s, LLVMBasicBlockRef after) {
	(void)after;
	LLVMValueRef target = expr_compile(s->sets.target);
	// dbg("Set target : %s", LLVMPrintValueToString(target));
	LLVMValueRef value = expr_compile(s->sets.value);
	if(s->sets.value->valueType == VALUE_STR) {
		if(LLVMGetTypeKind(LLVMTypeOf(value)) == LLVMArrayTypeKind) {
			size_t       length;
			LLVMValueRef vRef = LLVMBuildGlobalString(
			    globalBuilder, LLVMGetAsString(value, &length), "gString");
			LLVMValueRef idx[] = {LLVMConstInt(LLVMInt32Type(), 0, 0),
			                      LLVMConstInt(LLVMInt32Type(), 0, 0)};
			// value = LLVMBuildStore(globalBuilder, LLVMBuildGEP(globalBuilder,
			// vRef, idx, 2, "gStringGEP"), target);

			if(s->sets.target->valueType != VALUE_GEN)
				return LLVMBuildStore(
				    globalBuilder,
				    LLVMBuildGEP(globalBuilder, vRef, idx, 2, "gStringGEP"),
				    target);
			else
				value = vRef;
		} else if(LLVMIsAAllocaInst(value)) {
			value = LLVMBuildLoad(globalBuilder, value, "tmpStringLoad");
		}
	}
	if(s->sets.target->valueType == VALUE_GEN) {
		if(LLVMIsAAllocaInst(value))
			value = LLVMBuildLoad(globalBuilder, value, "tmpValLoad");
		LLVMTypeRef argType[] = {
		    LLVMPointerType(get_generic_structure_type(), 0), LLVMInt32Type(),
		    LLVMInt64Type()};
		LLVMTypeRef  fType = LLVMFunctionType(LLVMVoidType(), argType, 3, 0);
		LLVMValueRef fn;
		if((fn = LLVMGetNamedFunction(globalModule, "__algi_generic_store2")) ==
		   NULL)
			fn = LLVMAddFunction(globalModule, "__algi_generic_store2", fType);
		// runtime_function_used[ALGI_GENERIC_STORE] = 1;
		LLVMValueRef r[3];
		r[0] = target;
		r[1] = LLVMConstInt(LLVMInt32Type(), s->sets.value->valueType, 0);
		r[2] = LLVMGetTypeKind(LLVMTypeOf(value)) == LLVMPointerTypeKind
		           ? LLVMBuildPtrToInt(globalBuilder, value, LLVMInt64Type(),
		                               "tempPtrCast")
		           : LLVMTypeOf(value) == LLVMInt1Type()
		                 ? LLVMBuildIntCast(globalBuilder, value,
		                                    LLVMInt64Type(), "tmpBoolCast")
		                 : LLVMBuildBitCast(globalBuilder, value,
		                                    LLVMInt64Type(), "tempCast");
		return LLVMBuildCall(globalBuilder, fn, r, 3, "");
	}
	// if(LLVMTypeOf(target) != LLVMTypeOf(value)){
	//
	// }
	return LLVMBuildStore(globalBuilder, value, target);
}

static LLVMValueRef compile_if(Statement *s, LLVMBasicBlockRef after) {
	LLVMValueRef r = expr_compile(s->ifs.condition);
	// r = LLVMBuildFCmp(globalBuilder, LLVMRealOEQ, r,
	// LLVMConstReal(LLVMDoubleType(), 0.0), "ifcond");
	LLVMValueRef parent =
	    LLVMGetBasicBlockParent(LLVMGetInsertBlock(globalBuilder));

	LLVMBasicBlockRef mergeBB = LLVMAppendBasicBlock(parent, "ifcont");
	LLVMMoveBasicBlockAfter(mergeBB, after);
	LLVMBasicBlockRef elseBB = LLVMInsertBasicBlock(mergeBB, "else");
	LLVMBasicBlockRef thenBB = LLVMInsertBasicBlock(elseBB, "then");

	LLVMBuildCondBr(globalBuilder, r, thenBB, elseBB);

	LLVMPositionBuilderAtEnd(globalBuilder, thenBB);

	blockstmt_compile(s->ifs.thenBlock, 0, thenBB);

	LLVMBuildBr(globalBuilder, mergeBB);

	// thenBB = LLVMGetInsertBlock(globalBuilder);

	// LLVMInsertBasicBlock(elseBB, "else");
	LLVMPositionBuilderAtEnd(globalBuilder, elseBB);

	LLVMValueRef elseV = NULL;
	if(s->ifs.elseIf == NULL && s->ifs.elseBlock.count > 0)
		elseV = blockstmt_compile(s->ifs.elseBlock, 0, elseBB);
	else if(s->ifs.elseIf != NULL)
		elseV = statement_compile(s->ifs.elseIf, elseBB);

	LLVMBuildBr(globalBuilder, mergeBB);

	// elseBB = LLVMGetInsertBlock(globalBuilder);

	// LLVMInsertBasicBlock(mergeBB, "merge");
	LLVMPositionBuilderAtEnd(globalBuilder, mergeBB);

	// LLVMValueRef phi = LLVMBuildPhi(globalBuilder, LLVMDoubleType(),
	// "ifphi"); LLVMAddIncoming(phi, &thenV, &thenBB, 1); if(elseV != NULL)
	//    LLVMAddIncoming(phi, &elseV, &elseBB, 1);
	return LLVMConstInt(LLVMInt1Type(), 0, 0);
}

static LLVMValueRef compile_while(Statement *s, LLVMBasicBlockRef after) {

	LLVMValueRef parent =
	    LLVMGetBasicBlockParent(LLVMGetInsertBlock(globalBuilder));
	LLVMBasicBlockRef cont = LLVMAppendBasicBlock(parent, "wcont");
	LLVMMoveBasicBlockAfter(cont, after);

	LLVMBasicBlockRef wbody = LLVMInsertBasicBlock(cont, "wbody");
	LLVMBasicBlockRef wCond = LLVMInsertBasicBlock(wbody, "wcond");
	LLVMBuildBr(globalBuilder, wCond);
	LLVMPositionBuilderAtEnd(globalBuilder, wCond);

	LLVMValueRef condV = expr_compile(s->whiles.condition);

	LLVMBuildCondBr(globalBuilder, condV, wbody, cont);

	LLVMPositionBuilderAtEnd(globalBuilder, wbody);

	blockstmt_compile(s->whiles.statements, 0, wbody);

	LLVMBuildBr(globalBuilder, wCond);
	// LLVMBuildCondBr(globalBuilder, condV, wbody, cont);

	LLVMPositionBuilderAtEnd(globalBuilder, cont);
	// LLVMBuildRetVoid(globalBuilder);

	// LLVMBuildBr(globalBuilder, parent);

	return LLVMConstInt(LLVMInt1Type(), 0, 0);
}

static LLVMValueRef compile_do(Statement *s, LLVMBasicBlockRef after) {
	LLVMValueRef parent =
	    LLVMGetBasicBlockParent(LLVMGetInsertBlock(globalBuilder));
	LLVMBasicBlockRef cont = LLVMAppendBasicBlock(parent, "dcont");
	LLVMMoveBasicBlockAfter(cont, after);
	LLVMBasicBlockRef dbody = LLVMInsertBasicBlock(cont, "dbody");
	LLVMBuildBr(globalBuilder, dbody);
	LLVMPositionBuilderAtEnd(globalBuilder, dbody);
	blockstmt_compile(s->dos.statements, 0, dbody);
	LLVMValueRef cond = expr_compile(s->dos.condition);
	LLVMBuildCondBr(globalBuilder, cond, dbody, cont);
	LLVMPositionBuilderAtEnd(globalBuilder, cont);
	return LLVMConstInt(LLVMInt1Type(), 0, 0);
}

static LLVMValueRef compile_print(Statement *s, LLVMBasicBlockRef after) {
	(void)after;
	LLVMValueRef params[2];
	LLVMValueRef ret;
	for(uint64_t i = 0; i < s->prints.count; i++) {
		LLVMValueRef val    = expr_compile(s->prints.args[i]);
		LLVMTypeRef  valRef = LLVMVoidType();
		if(LLVMIsAAllocaInst(val)) {
			// dbg("It is alloca dude!\n");
			valRef = LLVMGetAllocatedType(val);
			val    = LLVMBuildLoad(globalBuilder, val, "valueLoad");
		} else
			valRef = LLVMTypeOf(val);
		LLVMTypeRef  paramTypes[2];
		LLVMValueRef idx[] = {LLVMConstInt(LLVMInt32Type(), 0, 0),
		                      LLVMConstInt(LLVMInt32Type(), 0, 0)};
		LLVMValueRef fspec;
		if(valRef == get_generic_structure_type()) {
			// fspec = LLVMBuildGlobalString(globalBuilder, "%g", "genspec");
			LLVMTypeRef  params[] = {get_generic_structure_type()};
			LLVMTypeRef  agvpt = LLVMFunctionType(LLVMVoidType(), params, 1, 0);
			LLVMValueRef ref;
			if((ref = LLVMGetNamedFunction(globalModule,
			                               "__algi_generic_print")) == NULL) {
				runtime_function_used[ALGI_GENERIC_PRINT] = 1;
				ref = LLVMAddFunction(globalModule, "__algi_generic_print",
				                      agvpt);
			}
			LLVMValueRef args[] = {val};
			return LLVMBuildCall(globalBuilder, ref, args, 1, "");
		} else if(valRef == LLVMInt64Type()) {
			if((fspec = LLVMGetNamedGlobal(globalModule, "intSpec")) == NULL)
				fspec =
				    LLVMBuildGlobalString(globalBuilder, "%" PRId64, "intSpec");

			paramTypes[1] = LLVMInt64Type();
			params[1]     = val;
		}

		else if(valRef == LLVMDoubleType()) {
			if((fspec = LLVMGetNamedGlobal(globalModule, "floatSpec")) == NULL)
				fspec =
				    LLVMBuildGlobalString(globalBuilder, "%.10g", "floatSpec");

			paramTypes[1] = LLVMDoubleType();
			params[1]     = val;
		} else {
			if((fspec = LLVMGetNamedGlobal(globalModule, "strSpec")) == NULL)
				fspec = LLVMBuildGlobalString(globalBuilder, "%s", "strSpec");

			paramTypes[1] = LLVMPointerType(LLVMInt8Type(), 0);

			if(valRef == LLVMPointerType(LLVMInt8Type(), 0))
				params[1] = val;
			else if(valRef == LLVMInt1Type()) {

				params[1] = LLVMBuildSelect(
				    globalBuilder, val,
				    LLVMBuildInBoundsGEP(globalBuilder,
				                         LLVMBuildGlobalString(globalBuilder,
				                                               "True\0",
				                                               "boolResT"),
				                         idx, 2, "gep"),
				    LLVMBuildInBoundsGEP(globalBuilder,
				                         LLVMBuildGlobalString(globalBuilder,
				                                               "False\0",
				                                               "boolResF"),
				                         idx, 2, "gep"),
				    "boolSelection");
				// params[1] = LLVMBuildInBoundsGEP(globalBuilder, params[1],
				// idx, 2, "str");
			} else {
				size_t      length;
				const char *str = LLVMGetAsString(val, &length);
				params[1] =
				    LLVMBuildGlobalString(globalBuilder, str, "pString");

				params[1] = LLVMBuildInBoundsGEP(globalBuilder, params[1], idx,
				                                 2, "str");
			}
		}

		params[0] = LLVMBuildInBoundsGEP(globalBuilder, fspec, idx, 2, "gep");
		paramTypes[0] = LLVMPointerType(LLVMInt8Type(), 0);

		LLVMTypeRef ftype = LLVMFunctionType(LLVMInt32Type(), paramTypes, 1, 1);

		LLVMValueRef pf;
		if((pf = LLVMGetNamedFunction(globalModule, "printf")) == NULL)
			pf = LLVMAddFunction(globalModule, "printf", ftype);

		// dbg("globalBuilder : %p\n");
		// dbg("params : %p\n", params);
		// dbg("pf : %p\n", pf);

		ret = LLVMBuildCall(globalBuilder, pf, params, 2, "pftemp");
	}
	return ret;
}

static LLVMValueRef statement_compile(Statement *s, LLVMBasicBlockRef after) {
	switch(s->type) {
		case STATEMENT_SET: {
			return compile_set(s, after);
		} break;
		case STATEMENT_IF: {
			return compile_if(s, after);
		} break;
		case STATEMENT_WHILE: {
			return compile_while(s, after);
		}
		case STATEMENT_DO: {
			return compile_do(s, after);
		}
		case STATEMENT_PRINT: {
			return compile_print(s, after);
		} break;
		default: return LLVMConstInt(LLVMInt1Type(), 1, 0);
	}
}

static LLVMValueRef func;

// If insertBlock in false, inblock contains the
// basic block in which the following statements are
// to be inserted
static LLVMValueRef blockstmt_compile(BlockStatement s, uint8_t insertBlock,
                                      LLVMBasicBlockRef inBlock) {
	AlgiContext bakContext = algiGlobalContext;

	if(!algiGlobalContextInit) {
		algiGlobalContextInit                = 1;
		algiGlobalContext.superContext       = NULL;
		algiGlobalContext.variableRefPointer = 0;
	} else {
		AlgiContext newcontext;
		newcontext.superContext       = &bakContext;
		newcontext.variableRefPointer = 0;
		algiGlobalContext             = newcontext;
	}

	LLVMBasicBlockRef bbr;
	LLVMBasicBlockRef bb = inBlock;
	if(insertBlock) {
		bbr = LLVMGetInsertBlock(globalBuilder);
		if(bbr != NULL) {
			dbg("GetInsertBlock : %p\n", bbr);
			LLVMValueRef bbp = LLVMGetBasicBlockParent(bbr);
			dbg("GetBasicBlockParent : %p\n", bbp);

			bb = LLVMAppendBasicBlock(bbp, "block");
		} else {
			dbg("Creating new block");
			bb = LLVMAppendBasicBlock(func, "block");
		}
		LLVMPositionBuilderAtEnd(globalBuilder, bb);
	}
	LLVMValueRef ret;
	for(uint64_t i = 0; i < s.count; i++) {
		ret = statement_compile(s.statements[i], bb);
	}

	algiGlobalContext = bakContext;

	if(insertBlock) {
		LLVMBuildRetVoid(globalBuilder);
		return LLVMBasicBlockAsValue(bb);
	} else
		return ret;
}

static void algi_show_err(const char *msg) {
	err("%s", msg);
}

#include "runtime_llvm.h"

void codegen_compile(BlockStatement bs) {
	globalModule = LLVMModuleCreateWithName("AlgiglobalModule");

	globalBuilder     = LLVMCreateBuilder();
	llvmGlobalContext = LLVMContextCreate();
	get_algi_generic_store(globalModule, globalBuilder);

	LLVMTypeRef ret_type = LLVMFunctionType(LLVMVoidType(), NULL, 0, 0);

	func                 = LLVMAddFunction(globalModule, "Main", ret_type);
	LLVMBasicBlockRef bb = LLVMAppendBasicBlock(func, "entry_point");
	LLVMPositionBuilderAtEnd(globalBuilder, bb);
	timer_start("Compilation");

	blockstmt_compile(bs, 0, bb);
	LLVMBuildRetVoid(globalBuilder);

	LLVMPositionBuilderAtEnd(globalBuilder, bb);

	LLVMVerifyFunction(func, LLVMReturnStatusAction);

	timer_end();

	char *err = NULL;
	dbg("Compiled code\n");
	LLVMDumpModule(globalModule);
	dbg("\n");
	int hasErr = LLVMVerifyModule(globalModule, LLVMReturnStatusAction, &err);
	if(hasErr)
		algi_show_err(err);
	LLVMDisposeMessage(err);

	if(hasErr) {
#ifdef DEBUG
		err("Press C to abort : ");
		char c = getc(stdin);
		if(c == 'c' || c == 'C')
#endif
			exit(EXIT_FAILURE);
	}

	err = NULL;

	timer_start("Initializing JIT");

	LLVMLinkInMCJIT();

	LLVMInitializeNativeTarget();
	// LLVMInitializeNativeAsmParser();
	LLVMInitializeNativeAsmPrinter();

	if(LLVMCreateExecutionEngineForModule(&globalEngine, globalModule, &err) !=
	   0) {
		printf("\nFailed to create execution engine!\n");
		abort();
	}
	if(err) {
		printf("\nError : %s\n", err);
		LLVMDisposeMessage(err);
		exit(EXIT_FAILURE);
	}

	// Map Algi runtime functions
	for(uint64_t i = 0; i < ALGI_RUNTIME_FUNCTION_COUNT; i++) {
		if(runtime_function_used[i]) {
			LLVMAddGlobalMapping(
			    globalEngine,
			    LLVMGetNamedFunction(globalModule,
			                         algi_runtime_functions[i].name),
			    algi_runtime_functions[i].address);
		}
	}

	LLVMSetModuleDataLayout(globalModule,
	                        LLVMGetExecutionEngineTargetData(globalEngine));

	timer_end();

	timer_start("Optimization");
	// Optimization
	LLVMPassManagerBuilderRef pmb = LLVMPassManagerBuilderCreate();
	LLVMPassManagerBuilderSetOptLevel(pmb, 3);
	LLVMPassManagerRef optimizer = LLVMCreatePassManager();

	LLVMPassManagerBuilderPopulateModulePassManager(pmb, optimizer);
	LLVMRunPassManager(optimizer, globalModule);
	timer_end();
	LLVMDisposePassManager(optimizer);
	LLVMPassManagerBuilderDispose(pmb);

#ifdef DEBUG
	dbg("Optimized code \n");
	LLVMDumpModule(globalModule);

	dbg("Press any key to run the program");
	dbg("Press C to cancel : ");
	char c = getc(stdin);
	if(c != 'c' && c != 'C') {
#endif

		void (*Main)(void) =
		    (void (*)(void))LLVMGetFunctionAddress(globalEngine, "Main");

#ifdef DEBUG
		timer_start("Execution");
#endif

		Main();

#ifdef DEBUG
		timer_end();
	} else
		warn("Run cancelled!");
#endif
}

void codegen_dispose() {
	LLVMDisposeBuilder(globalBuilder);
	if(globalEngine != NULL) {
		LLVMRunStaticDestructors(globalEngine);
		LLVMDisposeExecutionEngine(globalEngine);
	}
	LLVMShutdown();
	LLVMContextDispose(llvmGlobalContext);
}

void codegen_llvm_shutdown() {
	LLVMShutdown();
}
