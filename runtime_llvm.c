#include <llvm-c/Analysis.h>
#include <llvm-c/Core.h>
#include <llvm-c/ExecutionEngine.h>
#include <llvm-c/OrcBindings.h>
#include <llvm-c/Target.h>
#include <llvm-c/Types.h>

#include "display.h"
#include "types.h"

static LLVMTypeRef algi_generic_structure_type() {
	LLVMTypeRef types[] = {LLVMInt8Type(), LLVMInt64Type()};
	return LLVMStructType(types, 2, 0);
}

void get_algi_generic_store(LLVMModuleRef module, LLVMBuilderRef builder) {
	// char *err = NULL;
	// LLVMVerifyModule(module, LLVMReturnStatusAction, &err);

	LLVMTypeRef  types[] = {LLVMPointerType(algi_generic_structure_type(), 0),
                           LLVMInt32Type(), LLVMInt64Type()};
	LLVMTypeRef  args    = LLVMFunctionType(LLVMVoidType(), types, 3, 0);
	LLVMValueRef func = LLVMAddFunction(module, "__algi_generic_store2", args);
	LLVMBasicBlockRef coreblock = LLVMAppendBasicBlock(func, "ags_entry");
	LLVMPositionBuilderAtEnd(builder, coreblock);

	dbg("numParams : %u\n", LLVMCountParams(func));

	LLVMValueRef gpointer = LLVMGetParam(func, 0);
	LLVMValueRef gtype   = LLVMBuildStructGEP(builder, gpointer, 0, "mem_type");
	LLVMValueRef gvalue  = LLVMBuildStructGEP(builder, gpointer, 1, "mem_val");
	LLVMValueRef valtype = LLVMGetParam(func, 1);

	LLVMValueRef val = LLVMGetParam(func, 2);

	LLVMBasicBlockRef ext = LLVMAppendBasicBlock(func, "exit_store");

	LLVMPositionBuilderAtEnd(builder, coreblock);
	LLVMValueRef sval = LLVMBuildSwitch(builder, valtype, ext, 4);

	LLVMBasicBlockRef inCaseInt = LLVMInsertBasicBlock(ext, "inCaseInt");
	LLVMAddCase(sval, LLVMConstInt(LLVMInt32Type(), VALUE_INT, 0), inCaseInt);
	LLVMPositionBuilderAtEnd(builder, inCaseInt);
	LLVMBuildStore(builder, LLVMConstInt(LLVMInt8Type(), VALUE_INT, 0), gtype);
	LLVMBuildStore(builder, val, gvalue);
	LLVMBuildBr(builder, ext);
	// LLVMBuildRetVoid(builder);

	LLVMBasicBlockRef inCaseNum = LLVMInsertBasicBlock(ext, "inCaseNum");
	LLVMAddCase(sval, LLVMConstInt(LLVMInt32Type(), VALUE_NUM, 0), inCaseNum);
	LLVMPositionBuilderAtEnd(builder, inCaseNum);
	LLVMBuildStore(builder, LLVMConstInt(LLVMInt8Type(), VALUE_NUM, 0), gtype);
	LLVMBuildStore(builder,
	               LLVMBuildBitCast(builder, val, LLVMInt64Type(), "numtoint"),
	               gvalue);
	LLVMBuildBr(builder, ext);
	// LLVMBuildRetVoid(builder);

	LLVMBasicBlockRef inCaseGen = LLVMInsertBasicBlock(ext, "inCaseGen");
	LLVMAddCase(sval, LLVMConstInt(LLVMInt32Type(), VALUE_GEN, 0), inCaseGen);
	LLVMPositionBuilderAtEnd(builder, inCaseGen);
	LLVMValueRef ptr = LLVMBuildIntToPtr(
	    builder, val, LLVMPointerType(algi_generic_structure_type(), 0),
	    "ptrcast");
	LLVMValueRef readpointer = LLVMBuildStructGEP(builder, ptr, 0, "readtype");
	LLVMValueRef argGenType = LLVMBuildLoad(builder, readpointer, "arggentype");
	LLVMBuildStore(builder, argGenType, gtype);
	LLVMValueRef argGenVal = LLVMBuildLoad(
	    builder, LLVMBuildStructGEP(builder, ptr, 1, "readval"), "arggenval");
	LLVMBuildStore(
	    builder,
	    LLVMBuildBitCast(builder, argGenVal, LLVMInt64Type(), "ptrtoint"),
	    gvalue);
	LLVMBuildBr(builder, ext);
	// LLVMBuildRetVoid(builder);

	LLVMBasicBlockRef inCaseStr = LLVMInsertBasicBlock(inCaseGen, "inCaseStr");
	LLVMAddCase(sval, LLVMConstInt(LLVMInt32Type(), VALUE_STR, 0), inCaseStr);
	LLVMPositionBuilderAtEnd(builder, inCaseStr);
	LLVMBuildStore(builder, LLVMConstInt(LLVMInt8Type(), VALUE_STR, 0), gtype);
	LLVMBuildStore(builder,
	               LLVMBuildBitCast(builder, val, LLVMInt64Type(), "ptrtoint"),
	               gvalue);
	LLVMBuildBr(builder, ext);
	// LLVMBuildRetVoid(builder);

	LLVMPositionBuilderAtEnd(builder, ext);
	LLVMBuildRetVoid(builder);

	LLVMPositionBuilderAtEnd(builder, ext);

	// LLVMDumpModule(module);

	// dbg("\n%p %p %p %p %p\n", coreblock, inCaseInt, inCaseNum, inCaseStr,
	// inCaseGen); dbg("\nnumber of basic blocks : %u\n",
	// LLVMCountBasicBlocks(func));

	// LLVMVerifyFunction(func, LLVMReturnStatusAction);
	LLVMVerifyFunction(func, LLVMReturnStatusAction);
	// LLVMBuildRetVoid(builder);
}
