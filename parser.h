#pragma once

#include "lexer.h"
#include "stmt.h"
#include <stdbool.h>
#include <stdint.h>

BlockStatement parse(TokenList list);
uint64_t       parser_has_errors();
